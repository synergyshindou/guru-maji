package com.apps.majiguru.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.MessagesChat;
import com.apps.majiguru.R;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;

import java.util.List;

/**
 * Created by Dell_Cleva on 28/02/2018.
 */

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.FollowerViewHolder> {

    private static final String TAG = "ChatAdapter";

    private List<MessagesChat> chatList;
    private Context context;
    private int IDUser = 0;
    private SessionManager sessionManager;

    public ChatAdapter(Context context, List<MessagesChat> chatList) {
        this.chatList =chatList;
        this.context=context;
        sessionManager = new SessionManager(context);
        String d = sessionManager.getData();
        Data myData = Data.create(d);
        IDUser = Integer.parseInt(myData.getId());
    }

    @Override
    public FollowerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.row_chat, parent, false);
        return new FollowerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final FollowerViewHolder holder, final int position) {

        Log.i(TAG, "onBindViewHolder: "+chatList.get(position).getMessage());
        if (chatList.get(position).getUserGroup().getIdUser() == IDUser) {

            holder.layoutLeftMessages.setVisibility(View.GONE);
            holder.layoutRightMessages.setVisibility(View.VISIBLE);

            holder.messagesTextRight.setText(Html.fromHtml(chatList.get(position).getMessage()
                    + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
            holder.timeMessagesRight.setText(Method.FormatHourChat(chatList.get(position).getCreatedAt()));


        } else {

            holder.layoutLeftMessages.setVisibility(View.VISIBLE);
            holder.layoutRightMessages.setVisibility(View.GONE);

            holder.tvName.setText(chatList.get(position).getUserGroup().getUser().getFirstName());
            holder.messagesTextLeft.setText(Html.fromHtml(chatList.get(position).getMessage()
                    + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
            holder.timeMessagesLeft.setText(Method.FormatHourChat(chatList.get(position).getCreatedAt()));
        }
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }


    class FollowerViewHolder extends RecyclerView.ViewHolder {

        private TextView messagesTextLeft, timeMessagesLeft, messagesTextRight,timeMessagesRight, tvName;
        private LinearLayout layoutLeftMessages, layoutRightMessages;

        FollowerViewHolder(View convertView) {
            super(convertView);

            tvName =  convertView.findViewById(R.id.tvNameLeft);
            messagesTextLeft =  convertView.findViewById(R.id.text_message_left);
            timeMessagesLeft = convertView.findViewById(R.id.text_time_messages_left);
            messagesTextRight = convertView.findViewById(R.id.text_message_right);
            timeMessagesRight= convertView.findViewById(R.id.text_time_message_right);

            layoutLeftMessages = convertView.findViewById(R.id.layout_message_left);
            layoutRightMessages = convertView.findViewById(R.id.layout_message_right);
        }
    }
}
