package com.apps.majiguru.Utils;

/**
 * Created by Dell_Cleva on 28/01/2019.
 */

public class Constant {
    public static String voltedFontReguler = "";
    public static String QRID = "qrid";
    public static int QRREGISTER = 101;
    public static String LINKSERVER = "https://classmiles.com/engine/maghrib_mengaji/user/login";
    public static String BASE_URL = "https://maghribmengaji.id/api/v1/";
    public static String BASE_URL_CHAT = "https://extengine.classmiles.com/chat_engine_maji/";
    static String CHAT_SERVER_URL = "https://extengine.classmiles.com:6001";
    public static String CHAT_MESSAGE = "chat message";

    public static String PLAY = "play";
    public static String WAIT = "wait";
    public static String PASS = "pass";
    public static String ERROR = "error";
}
