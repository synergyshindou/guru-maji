package com.apps.majiguru.Utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.apps.majiguru.MainActivity;

/**
 * Created by Dell_Cleva on 06/02/2019.
 */

public class SessionManager {

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "MengajiPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_TOKEN_FCM = "token_fcm";
    public static final String KEY_TOKEN_CHAT = "token_chat";
    public static final String IS_HAS_FCM = "token_fcms";
    public static final String KEY_DATA = "data";
    public static final String KEY_DATA_PROFILE = "dataprofile";

    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public String getToken(){
        return pref.getString(KEY_TOKEN, "");
    }

    public String getTokenFCM() {
        return pref.getString(KEY_TOKEN_FCM, "kosong");
    }

    public String getTokenChat() {
        return pref.getString(KEY_TOKEN_CHAT, "");
    }

    public boolean isTokenFCM() {
        return pref.getBoolean(IS_HAS_FCM, false);
    }

    public String getData(){
        return pref.getString(KEY_DATA, null);
    }

    public String getDataProfile(){
        return pref.getString(KEY_DATA_PROFILE, null);
    }

    public void createToken(String tokenfcm) {
        editor.putString(KEY_TOKEN, tokenfcm);
        editor.commit();
    }

    public void createTokenFCM(String tokenfcm) {
        editor.putString(KEY_TOKEN_FCM, tokenfcm);
        editor.commit();
    }

    public void createTokenFCM() {
        editor.putBoolean(IS_HAS_FCM, true);
        editor.commit();
    }

    public void createDataProfile(String serializedData) {
        editor.remove(KEY_DATA_PROFILE);
        editor.putString(KEY_DATA_PROFILE, serializedData);
        editor.commit();
    }

    public void createData(String serializedData) {
        editor.putString(KEY_DATA, serializedData);

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);
        editor.commit();
    }

    public void createTokenChat(String tokenchat) {
        editor.putString(KEY_TOKEN_CHAT, tokenchat);
        editor.commit();
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, MainActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
