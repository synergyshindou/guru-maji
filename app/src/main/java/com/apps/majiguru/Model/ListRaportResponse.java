
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListRaportResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Dataraport> dataraport = new ArrayList<Dataraport>();

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ListRaportResponse{");
        sb.append("status=").append(status);
        sb.append(", code=").append(code);
        sb.append(", message='").append(message).append('\'');
        sb.append(", dataraport=").append(dataraport);
        sb.append('}');
        return sb.toString();
    }

    public final static Creator<ListRaportResponse> CREATOR = new Creator<ListRaportResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ListRaportResponse createFromParcel(Parcel in) {
            ListRaportResponse instance = new ListRaportResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.dataraport, (com.apps.majiguru.Model.Dataraport.class.getClassLoader()));
            return instance;
        }

        public ListRaportResponse[] newArray(int size) {
            return (new ListRaportResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataraport
     */
    public List<Dataraport> getDataraport() {
        return dataraport;
    }

    /**
     * 
     * @param dataraport
     *     The dataraport
     */
    public void setDataraport(List<Dataraport> dataraport) {
        this.dataraport = dataraport;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeList(dataraport);
    }

    public int describeContents() {
        return  0;
    }

}
