
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum implements Parcelable {

    @SerializedName("id_student")
    @Expose
    private String idStudent;
    @SerializedName("id_teacher")
    @Expose
    private String idTeacher;
    @SerializedName("id_school")
    @Expose
    private String idSchool;
    @SerializedName("kelas")
    @Expose
    private String kelas;
    @SerializedName("no_induk")
    @Expose
    private String noInduk;
    @SerializedName("nisn")
    @Expose
    private String nisn;
    @SerializedName("tahun_ajaran")
    @Expose
    private String tahunAjaran;
    @SerializedName("pass")
    @Expose
    private String pass;
    @SerializedName("name_student")
    @Expose
    private String nameStudent;
    @SerializedName("nip")
    @Expose
    private String nip;
    @SerializedName("nrk")
    @Expose
    private String nrk;
    @SerializedName("nuptk")
    @Expose
    private String nuptk;
    @SerializedName("name_teacher")
    @Expose
    private String nameTeacher;
    @SerializedName("place_of_birth")
    @Expose
    private String placeOfBirth;
    @SerializedName("birthday")
    @Expose
    private String birthday;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("job")
    @Expose
    private String job;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("jk")
    @Expose
    private String jk;
    public final static Parcelable.Creator<Datum> CREATOR = new Creator<Datum>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Datum createFromParcel(Parcel in) {
            Datum instance = new Datum();
            instance.idStudent = ((String) in.readValue((String.class.getClassLoader())));
            instance.idTeacher = ((String) in.readValue((String.class.getClassLoader())));
            instance.idSchool = ((String) in.readValue((String.class.getClassLoader())));
            instance.kelas = ((String) in.readValue((String.class.getClassLoader())));
            instance.noInduk = ((String) in.readValue((String.class.getClassLoader())));
            instance.nisn = ((String) in.readValue((String.class.getClassLoader())));
            instance.tahunAjaran = ((String) in.readValue((String.class.getClassLoader())));
            instance.pass = ((String) in.readValue((String.class.getClassLoader())));
            instance.nameStudent = ((String) in.readValue((String.class.getClassLoader())));
            instance.nip = ((String) in.readValue((String.class.getClassLoader())));
            instance.nrk = ((String) in.readValue((String.class.getClassLoader())));
            instance.nuptk = ((String) in.readValue((String.class.getClassLoader())));
            instance.nameTeacher = ((String) in.readValue((String.class.getClassLoader())));
            instance.placeOfBirth = ((String) in.readValue((String.class.getClassLoader())));
            instance.birthday = ((String) in.readValue((String.class.getClassLoader())));
            instance.section = ((String) in.readValue((String.class.getClassLoader())));
            instance.job = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.userImage = ((String) in.readValue((String.class.getClassLoader())));
            instance.jk = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public Datum[] newArray(int size) {
            return (new Datum[size]);
        }

    };

    /**
     * @return The idStudent
     */
    public String getIdStudent() {
        return idStudent;
    }

    /**
     * @param idStudent The id_student
     */
    public void setIdStudent(String idStudent) {
        this.idStudent = idStudent;
    }

    /**
     * @return The idTeacher
     */
    public String getIdTeacher() {
        return idTeacher;
    }

    /**
     * @param idTeacher The id_teacher
     */
    public void setIdTeacher(String idTeacher) {
        this.idTeacher = idTeacher;
    }

    /**
     * @return The idSchool
     */
    public String getIdSchool() {
        return idSchool;
    }

    /**
     * @param idSchool The id_school
     */
    public void setIdSchool(String idSchool) {
        this.idSchool = idSchool;
    }

    /**
     * @return The kelas
     */
    public String getKelas() {
        return kelas;
    }

    /**
     * @param kelas The kelas
     */
    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    /**
     * @return The noInduk
     */
    public String getNoInduk() {
        return noInduk;
    }

    /**
     * @param noInduk The no_induk
     */
    public void setNoInduk(String noInduk) {
        this.noInduk = noInduk;
    }

    /**
     * @return The nisn
     */
    public String getNisn() {
        return nisn;
    }

    /**
     * @param nisn The nisn
     */
    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    /**
     * @return The tahunAjaran
     */
    public String getTahunAjaran() {
        return tahunAjaran;
    }

    /**
     * @param tahunAjaran The tahun_ajaran
     */
    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }

    /**
     * @return The pass
     */
    public String getPass() {
        return pass;
    }

    /**
     * @param pass The pass
     */
    public void setPass(String pass) {
        this.pass = pass;
    }

    /**
     * @return The nameStudent
     */
    public String getNameStudent() {
        return nameStudent;
    }

    /**
     * @param nameStudent The name_student
     */
    public void setNameStudent(String nameStudent) {
        this.nameStudent = nameStudent;
    }

    /**
     * @return The nip
     */
    public String getNip() {
        return nip;
    }

    /**
     * @param nip The nip
     */
    public void setNip(String nip) {
        this.nip = nip;
    }

    /**
     * @return The nrk
     */
    public String getNrk() {
        return nrk;
    }

    /**
     * @param nrk The nrk
     */
    public void setNrk(String nrk) {
        this.nrk = nrk;
    }

    /**
     * @return The nuptk
     */
    public String getNuptk() {
        return nuptk;
    }

    /**
     * @param nuptk The nuptk
     */
    public void setNuptk(String nuptk) {
        this.nuptk = nuptk;
    }

    /**
     * @return The nameTeacher
     */
    public String getNameTeacher() {
        return nameTeacher;
    }

    /**
     * @param nameTeacher The name_teacher
     */
    public void setNameTeacher(String nameTeacher) {
        this.nameTeacher = nameTeacher;
    }

    /**
     * @return The placeOfBirth
     */
    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    /**
     * @param placeOfBirth The place_of_birth
     */
    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    /**
     * @return The birthday
     */
    public String getBirthday() {
        return birthday;
    }

    /**
     * @param birthday The birthday
     */
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    /**
     * @return The section
     */
    public String getSection() {
        return section;
    }

    /**
     * @param section The section
     */
    public void setSection(String section) {
        this.section = section;
    }

    /**
     * @return The job
     */
    public String getJob() {
        return job;
    }

    /**
     * @param job The job
     */
    public void setJob(String job) {
        this.job = job;
    }

    /**
     * @return The createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idStudent);
        dest.writeValue(idTeacher);
        dest.writeValue(idSchool);
        dest.writeValue(kelas);
        dest.writeValue(noInduk);
        dest.writeValue(nisn);
        dest.writeValue(tahunAjaran);
        dest.writeValue(pass);
        dest.writeValue(nameStudent);
        dest.writeValue(nip);
        dest.writeValue(nrk);
        dest.writeValue(nuptk);
        dest.writeValue(nameTeacher);
        dest.writeValue(placeOfBirth);
        dest.writeValue(birthday);
        dest.writeValue(section);
        dest.writeValue(job);
        dest.writeValue(createdAt);
        dest.writeValue(userImage);
        dest.writeValue(jk);
    }

    public int describeContents() {
        return 0;
    }

}
