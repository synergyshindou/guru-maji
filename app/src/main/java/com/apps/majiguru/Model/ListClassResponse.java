
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListClassResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ListClass")
    @Expose
    private List<ListClas> ListClass = new ArrayList<ListClas>();
    public final static Creator<ListClassResponse> CREATOR = new Creator<ListClassResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ListClassResponse createFromParcel(Parcel in) {
            ListClassResponse instance = new ListClassResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.ListClass, (com.apps.majiguru.Model.ListClas.class.getClassLoader()));
            return instance;
        }

        public ListClassResponse[] newArray(int size) {
            return (new ListClassResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The ListClass
     */
    public List<ListClas> getListClass() {
        return ListClass;
    }

    /**
     * 
     * @param ListClass
     *     The ListClass
     */
    public void setListClass(List<ListClas> ListClass) {
        this.ListClass = ListClass;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeList(ListClass);
    }

    public int describeContents() {
        return  0;
    }

}
