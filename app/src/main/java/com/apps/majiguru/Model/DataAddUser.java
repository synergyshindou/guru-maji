
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DataAddUser implements Parcelable
{

    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("id_group")
    @Expose
    private String idGroup;
    @SerializedName("id")
    @Expose
    private int id;
    public final static Creator<DataAddUser> CREATOR = new Creator<DataAddUser>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DataAddUser createFromParcel(Parcel in) {
            DataAddUser instance = new DataAddUser();
            instance.idUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.idGroup = ((String) in.readValue((String.class.getClassLoader())));
            instance.id = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public DataAddUser[] newArray(int size) {
            return (new DataAddUser[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * 
     * @param idUser
     *     The id_user
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * 
     * @return
     *     The idGroup
     */
    public String getIdGroup() {
        return idGroup;
    }

    /**
     * 
     * @param idGroup
     *     The id_group
     */
    public void setIdGroup(String idGroup) {
        this.idGroup = idGroup;
    }

    /**
     * 
     * @return
     *     The id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @param id
     *     The id
     */
    public void setId(int id) {
        this.id = id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idUser);
        dest.writeValue(idGroup);
        dest.writeValue(id);
    }

    public int describeContents() {
        return  0;
    }

}
