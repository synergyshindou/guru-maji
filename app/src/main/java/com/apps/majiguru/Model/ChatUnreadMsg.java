
package com.apps.majiguru.Model;

import java.util.ArrayList;
import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChatUnreadMsg implements Parcelable
{

    @Override
    public String toString() {
        return "ChatUnreadMsg{" +
                "status='" + status + '\'' +
                ", messageUnreads=" + messageUnreads +
                '}';
    }

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("messages")
    @Expose
    private List<MessageUnread> messageUnreads = new ArrayList<MessageUnread>();
    public final static Parcelable.Creator<ChatUnreadMsg> CREATOR = new Creator<ChatUnreadMsg>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ChatUnreadMsg createFromParcel(Parcel in) {
            ChatUnreadMsg instance = new ChatUnreadMsg();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.messageUnreads, (MessageUnread.class.getClassLoader()));
            return instance;
        }

        public ChatUnreadMsg[] newArray(int size) {
            return (new ChatUnreadMsg[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The messageUnreads
     */
    public List<MessageUnread> getMessageUnreads() {
        return messageUnreads;
    }

    /**
     * 
     * @param messageUnreads
     *     The messageUnreads
     */
    public void setMessageUnreads(List<MessageUnread> messageUnreads) {
        this.messageUnreads = messageUnreads;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(messageUnreads);
    }

    public int describeContents() {
        return  0;
    }

}
