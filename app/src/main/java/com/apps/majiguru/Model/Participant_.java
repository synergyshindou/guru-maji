
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Participant_ implements Parcelable
{

    @SerializedName("id_user")
    @Expose
    private String idUser;
    @SerializedName("st")
    @Expose
    private int st;
    public final static Creator<Participant_> CREATOR = new Creator<Participant_>() {


        @SuppressWarnings({
            "unchecked"
        })
        public Participant_ createFromParcel(Parcel in) {
            Participant_ instance = new Participant_();
            instance.idUser = ((String) in.readValue((String.class.getClassLoader())));
            instance.st = ((int) in.readValue((int.class.getClassLoader())));
            return instance;
        }

        public Participant_[] newArray(int size) {
            return (new Participant_[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The idUser
     */
    public String getIdUser() {
        return idUser;
    }

    /**
     * 
     * @param idUser
     *     The id_user
     */
    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    /**
     * 
     * @return
     *     The st
     */
    public int getSt() {
        return st;
    }

    /**
     * 
     * @param st
     *     The st
     */
    public void setSt(int st) {
        this.st = st;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(idUser);
        dest.writeValue(st);
    }

    public int describeContents() {
        return  0;
    }

}
