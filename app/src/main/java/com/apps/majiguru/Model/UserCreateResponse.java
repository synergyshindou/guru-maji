
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserCreateResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private DataUserChat dataUserChat;
    public final static Creator<UserCreateResponse> CREATOR = new Creator<UserCreateResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public UserCreateResponse createFromParcel(Parcel in) {
            UserCreateResponse instance = new UserCreateResponse();
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            instance.dataUserChat = ((DataUserChat) in.readValue((DataUserChat.class.getClassLoader())));
            return instance;
        }

        public UserCreateResponse[] newArray(int size) {
            return (new UserCreateResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataUserChat
     */
    public DataUserChat getDataUserChat() {
        return dataUserChat;
    }

    /**
     * 
     * @param dataUserChat
     *     The dataUserChat
     */
    public void setDataUserChat(DataUserChat dataUserChat) {
        this.dataUserChat = dataUserChat;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeValue(dataUserChat);
    }

    public int describeContents() {
        return  0;
    }

}
