
package com.apps.majiguru.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListGuruResponse implements Parcelable
{

    @SerializedName("status")
    @Expose
    private boolean status;
    @SerializedName("code")
    @Expose
    private int code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DataGuru> dataGuru = new ArrayList<DataGuru>();
    public final static Creator<ListGuruResponse> CREATOR = new Creator<ListGuruResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ListGuruResponse createFromParcel(Parcel in) {
            ListGuruResponse instance = new ListGuruResponse();
            instance.status = ((boolean) in.readValue((boolean.class.getClassLoader())));
            instance.code = ((int) in.readValue((int.class.getClassLoader())));
            instance.message = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.dataGuru, (com.apps.majiguru.Model.DataGuru.class.getClassLoader()));
            return instance;
        }

        public ListGuruResponse[] newArray(int size) {
            return (new ListGuruResponse[size]);
        }

    }
    ;

    /**
     * 
     * @return
     *     The status
     */
    public boolean isStatus() {
        return status;
    }

    /**
     * 
     * @param status
     *     The status
     */
    public void setStatus(boolean status) {
        this.status = status;
    }

    /**
     * 
     * @return
     *     The code
     */
    public int getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(int code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * 
     * @param message
     *     The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * 
     * @return
     *     The dataGuru
     */
    public List<DataGuru> getDataGuru() {
        return dataGuru;
    }

    /**
     * 
     * @param dataGuru
     *     The dataGuru
     */
    public void setDataGuru(List<DataGuru> dataGuru) {
        this.dataGuru = dataGuru;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(code);
        dest.writeValue(message);
        dest.writeList(dataGuru);
    }

    public int describeContents() {
        return  0;
    }

}
