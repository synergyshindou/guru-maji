package com.apps.majiguru;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.LoginResponse;
import com.apps.majiguru.Model.RegisterResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.apps.majiguru.Utils.Constant.QRID;
import static com.apps.majiguru.Utils.Constant.QRREGISTER;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    RadioGroup radioGroup;
    RadioButton radioSexButton;
    TextInputEditText etName, etEmail, etSandi, etSandiKonfirm, etPhone;
    TextInputEditText etEmailLogin, etPassLogin, etEmailF;
    TextView tvVersion;
    View llDaftar, llLogin,llForgot, mProgressView;

    ImageView imgHead;
    ApiConfig mAPIService;

    String rgender;

    SessionManager sessionManager;

    int back = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sessionManager = new SessionManager(this);
        if (sessionManager.isLoggedIn()) {

            Intent i = new Intent(MainActivity.this, HomeActivity.class);
            if (getIntent().hasExtra("action"))
                i.putExtra("action", getIntent().getStringExtra("action"));
            startActivity(i);
            finish();
            return;
        }
        InitView();
        mAPIService = AppConfig.getRetrofit().create(ApiConfig.class);
    }

    @Override
    public void onBackPressed() {
        if (back == 1) {
            llDaftar.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            back = 0;
        } else if (back == 2) {
            llForgot.setVisibility(View.GONE);
            llLogin.setVisibility(View.VISIBLE);
            back = 0;
        } else
            super.onBackPressed();
    }

    public void InitView(){
        llDaftar = findViewById(R.id.llDaftar);
        llLogin = findViewById(R.id.llLogin);
        llForgot = findViewById(R.id.llForgot);
        mProgressView = findViewById(R.id.login_progress);

        etEmailLogin = findViewById(R.id.etEmailLogin);
        etPassLogin = findViewById(R.id.etPass);
        etPassLogin.setTransformationMethod(new PasswordTransformationMethod());
        etPassLogin.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etPassLogin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    doLogin();
                    return true;
                }
                return false;
            }
        });

        etName = findViewById(R.id.etNama);
        etEmail = findViewById(R.id.etEmail);
        etEmailF = findViewById(R.id.etEmailForgot);
        etSandi = findViewById(R.id.etSandi);
        etSandiKonfirm = findViewById(R.id.etSandiKonfirm);
        etPhone = findViewById(R.id.etTelepon);
        radioGroup = findViewById(R.id.radioGroup1);

        tvVersion = findViewById(R.id.tvVersion);
        tvVersion.setText("v"+ Method.checkVersion(this));

        imgHead = findViewById(R.id.imgHeads);

        Glide.with(this)
                .load(R.drawable.mosque_blank)
                .into(imgHead);

    }

    public void sendData(View v){
        if (etName.getText().toString().length() == 0){
            etName.setError("Tidak Boleh Kosong");
            etName.requestFocus();
        } else {
            showProgressD(true);
            int selectedId = radioGroup.getCheckedRadioButtonId();
            radioSexButton = findViewById(selectedId);
            if (radioSexButton.getText().equals("Pria"))
                rgender = "L";
            else
                rgender = "P";
            mAPIService.register(etEmail.getText().toString(),
                    etSandiKonfirm.getText().toString(),
                    etName.getText().toString(),
                    rgender,
                    etPhone.getText().toString(),
                    "tutor")
                    .enqueue(new Callback<ServerResponse>() {
                        @Override
                        public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                            Log.i(TAG, "onResponse: M "+response.message());
                            if (response.body() != null){
                                Log.i(TAG, "onResponse: B "+response.body());

                                if (response.body().isSuccess()){
                                    showProgress(false);
                                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                    showProgressD(false);
                                }
                            } else
                                Toast.makeText(MainActivity.this, "Data error", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onFailure(Call<ServerResponse> call, Throwable t) {
                            Log.e(TAG, "onFailure: " + t.getMessage() );
                            Toast.makeText(MainActivity.this, "Something error, please try again", Toast.LENGTH_SHORT).show();
//                            Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                            showProgressD(false);
                        }
                    });
        }
    }

    public void sendForgot(View v) {
        if (etEmailF.getText().toString().length() == 0) {
            etEmailF.setError("Tidak Boleh Kosong");
            etEmailF.requestFocus();
        } else {
            showProgressF(true);
            Log.i(TAG, "sendForgot: To server");
            mAPIService.forgotPassword(etEmailF.getText().toString().trim()).enqueue(new Callback<ServerResponse>() {
                @Override
                public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                    Log.i(TAG, "onResponse: M " + response.message());
                    showProgressF(false);
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: B " + response.body());
                        if (response.body().isSuccess()) {
                            showDialog(response.body().getMessage());
                            llForgot.setVisibility(View.GONE);
                            llLogin.setVisibility(View.VISIBLE);
                            back = 0;
                        } else {
                            showDialog(response.body().getMessage());
                        }
                    } else {
                        Toast.makeText(MainActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<ServerResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    Toast.makeText(MainActivity.this, "Check your connection and try again. . .", Toast.LENGTH_SHORT).show();
                    showProgressF(false);
                }
            });
        }
    }

    public void doLogin(){
        if (etEmailLogin.getText().toString().length() == 0){
            etEmailLogin.setError("Tidak Boleh Kosong");
            etEmailLogin.requestFocus();
        } else if (etPassLogin.getText().toString().length() == 0){
            etPassLogin.setError("Tidak Boleh Kosong");
            etPassLogin.requestFocus();
        } else {
            showProgress(true);
            mAPIService.login(etEmailLogin.getText().toString().trim(), etPassLogin.getText().toString().trim()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.i(TAG, "onResponse: M "+response.message());
                    if (response.body() != null){
                        Log.i(TAG, "onResponse: B "+response.body());
                        if (response.body().isStatus()){
                            if (response.body().getData() != null) {
                                Data data = response.body().getData();
                                if (data.getUsertype().equals("tutor")) {
                                    String serialize = data.serialize();
                                    Log.i(TAG, "onResponse: Data SERVER " + data.getEmail());
                                    sessionManager.createData(serialize);
                                    sessionManager.createToken(response.body().getAccessToken());
                                    saveFCM(data.getId());
                                } else {
                                    showDialog("Email Anda sudah terdaftar sebagai akun Siswa.");
                                    showProgress(false);
                                }
                            } else {
                                showDialog(response.body().getMessage());
                                showProgress(false);
                            }
                        } else {
                            if (response.body().getCode() == 403)
                                showDialogSendMail(response.body().getMessage());
                            else
                                showDialog(response.body().getMessage());
                            showProgress(false);
                        }
                    } else {
                        showDialog("Please try again. . .");
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e(TAG, "onFailure: " + t.getMessage() );
                    showDialog("Check connection and try again. . .");
                    showProgress(false);

                }
            });
        }
    }

    void saveFCM(String iduser){
        mAPIService.sendFCM(iduser, sessionManager.getTokenFCM(), sessionManager.getToken()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {

                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }
                sessionManager.createTokenFCM();
                startActivity(new Intent(MainActivity.this, HomeActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                showProgress(false);
                showDialog("Check connection and try again. . .");
            }
        });
    }

    public void onDaftar(View v){
        llDaftar.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        back = 1;
    }

    public void onForgot(View v) {
        llForgot.setVisibility(View.VISIBLE);
        llLogin.setVisibility(View.GONE);
        back = 2;
    }

    public void onLogin(View v){
        doLogin();
    }

    public void onScanQR(View v){
//        startActivityForResult(new Intent(MainActivity.this, QRScanActivity.class), QRREGISTER);
    }

    void resendEmail(){
        mAPIService.resendActivation(etEmailLogin.getText().toString().trim()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                showProgress(false);
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                    if (response.body().isSuccess()) {
                        showDialog(response.body().getMessage());
                    } else {
                        showDialog(response.body().getMessage());
                    }
                } else {
                    showDialog("please try again. . .");
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                showDialog("Check connection and try again. . .");
                showProgress(false);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QRREGISTER){
            if (resultCode == RESULT_OK){
                Toast.makeText(this, data.getStringExtra(QRID), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llLogin.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showProgressD(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llDaftar.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void showProgressF(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        llForgot.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public void showDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.show();
    }

    public void showDialogSendMail(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.setNegativeButton("Resend Email", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showProgress(true);
                resendEmail();
            }
        });
        builder.show();
    }
}
