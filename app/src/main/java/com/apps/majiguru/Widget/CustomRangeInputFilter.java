package com.apps.majiguru.Widget;

import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;

public class CustomRangeInputFilter implements InputFilter {
    private static final String TAG = "CustomRangeInputFilter";
    private int minValue;
    private int maxValue;

    public CustomRangeInputFilter(int minVal, int maxVal) {
        this.minValue = minVal;
        this.maxValue = maxVal;
    }

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dStart, int dEnd) {
        try {
            // Remove the string out of destination that is to be replaced
            String newVal = dest.toString().substring(0, dStart) + dest.toString().substring(dEnd, dest.toString().length());
            newVal = newVal.substring(0, dStart) + source.toString() + newVal.substring(dStart, newVal.length());

            int input = Integer.parseInt(dest.toString() + source.toString());

            Log.i(TAG, "filter: " + input);
            Log.w(TAG, "filter: dest " + dest +"|"+dest.toString().equals("0"));
            Log.v(TAG, "filter: source " + source);

            if (dest.toString().equals("0")){
                Log.e(TAG, "filter: 0 dua kali not");
                return "";
            }

            if (isInRange(minValue, maxValue, input)) {
                return null;
            }


        } catch (NumberFormatException e) {
            e.printStackTrace();
            return "";
        }
        return "";
    }

    private boolean isInRange(int a, int b, int c) {
        return b > a ? c >= a && c <= b : c >= b && c <= a;
    }
}
