package com.apps.majiguru.Widget;

/**
 * Created by Dell_Cleva on 06/02/2019.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.apps.majiguru.R;


public class MyCheckBox extends android.widget.CheckBox {

    private String mFontPath;

    public MyCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RadioButton);
        int count = typedArray.getIndexCount();

        try {
            for (int i = 0; i < count; ++i) {
                int attr = typedArray.getIndex(i);
                if (attr == R.styleable.RadioButton_font_path)
                    mFontPath = typedArray.getString(attr);
            }
            if (mFontPath != null && !mFontPath.isEmpty()) {
                Typeface typeface = TypefaceManager.getInstance().getTypeface(context, mFontPath);
                setTypeface(typeface);
            }
        } finally {
            typedArray.recycle();
        }

        setTransformationMethod(null);
    }
}