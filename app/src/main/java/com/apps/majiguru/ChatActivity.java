package com.apps.majiguru;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.apps.majiguru.Adapter.ChatAdapter;
import com.apps.majiguru.Model.ChatReceiveMsg;
import com.apps.majiguru.Model.ChatSendMsgResp;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.MessagesChat;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.MyApplication;
import com.apps.majiguru.Utils.SessionManager;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends AppCompatActivity {

    private static final String TAG = "ChatActivity";
    ChatAdapter chatAdapter;
    RecyclerView mRecyclerView;
    EditText etChat;
    List<MessagesChat> chatPojos;
    public ApiConfig restApiChat;
    public Data myData;
    public SessionManager sessionManager;
    public Boolean isConnected = true;
    public Socket mSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setTitle("Chat");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Button btnSend = findViewById(R.id.button_send_message);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClickSend(v);
            }
        });
        sessionManager = new SessionManager(this);
        restApiChat = AppConfig.getRetrofitChat().create(ApiConfig.class);

        String d = sessionManager.getData();
        myData = Data.create(d);
        etChat = findViewById(R.id.edittext_chat_message);
        etChat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mRecyclerView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        scrollToBottom();
                    }
                }, 200);
                scrollToBottom();
            }
        });

        MyApplication app = (MyApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Constant.CHAT_MESSAGE, onChatMessage);
        mSocket.connect();

        chatPojos = new ArrayList<>();

        mRecyclerView = findViewById(R.id.recycler_viewChat);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplication()));
        chatAdapter = new ChatAdapter(getApplication(), chatPojos);
        mRecyclerView.setAdapter(chatAdapter);
        scrollToBottom();
        onReceive();
        onRead();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void ClickSend(View v) {
        String message = etChat.getText().toString();
        if (!message.trim().equals("")) {
            sumbitMsg(message);
        }
        etChat.setText("");
    }

    public void sumbitMsg(final String msg) {
        restApiChat.chatSendMsg(myData.getId(), myData.getId(), msg).enqueue(new Callback<ChatSendMsgResp>() {
            @Override
            public void onResponse(Call<ChatSendMsgResp> call, Response<ChatSendMsgResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {
                            mSocket.emit(Constant.CHAT_MESSAGE, msg);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatSendMsgResp> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: sumbitMsg "+t.getMessage() );
            }
        });
    }

    public void onReceive() {
        restApiChat.chatReceiveMsg(myData.getId()).enqueue(new Callback<ChatReceiveMsg>() {
            @Override
            public void onResponse(Call<ChatReceiveMsg> call, Response<ChatReceiveMsg> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        chatPojos.clear();
                        chatPojos.addAll(response.body().getMessagesChat());

                        chatAdapter.notifyDataSetChanged();
                        scrollToBottom();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatReceiveMsg> call, Throwable t) {
                t.printStackTrace();
                Log.e(TAG, "onFailure: onReceive "+t.getMessage() );

            }
        });
    }

    public void onRead() {
        restApiChat.chatReadMsg(myData.getId(), myData.getId()).enqueue(new Callback<ChatSendMsgResp>() {
            @Override
            public void onResponse(Call<ChatSendMsgResp> call, Response<ChatSendMsgResp> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i(TAG, "onResponse: "+response.body().toString());
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatSendMsgResp> call, Throwable t) {
                Log.e(TAG, "onFailure: onReceive");
                t.printStackTrace();

            }
        });
    }

    private void scrollToBottom() {
        mRecyclerView.scrollToPosition(chatPojos.size() - 1);
    }

    // SOCKET EMIT

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "run: connected");
                    if (!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };

    private Emitter.Listener onChatMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "run: chatMessage");
                    onReceive();
                }
            });
        }
    };
}
