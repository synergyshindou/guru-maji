package com.apps.majiguru.Rest;

/**
 * Created by delaroystudios on 10/5/2016.
 */

import com.apps.majiguru.Model.ChatAddUserResponse;
import com.apps.majiguru.Model.ChatGroupCreateResponse;
import com.apps.majiguru.Model.ChatLoginResponse;
import com.apps.majiguru.Model.ChatReceiveMsg;
import com.apps.majiguru.Model.ChatSendMsgResp;
import com.apps.majiguru.Model.ChatUnreadMsg;
import com.apps.majiguru.Model.CheckVersionResponse;
import com.apps.majiguru.Model.ListClassResponse;
import com.apps.majiguru.Model.ListGuruResponse;
import com.apps.majiguru.Model.ListMengajiResponse;
import com.apps.majiguru.Model.ListPesertaResponse;
import com.apps.majiguru.Model.ListRaportResponse;
import com.apps.majiguru.Model.ListTopMengaji;
import com.apps.majiguru.Model.LoginResponse;
import com.apps.majiguru.Model.ProfileResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.Model.UserCreateResponse;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;


public interface ApiConfig {

    @FormUrlEncoded
    @POST("AuthLogin")
    Call<LoginResponse> login(@Field("name") String name, @Field("password") String pass);

    @FormUrlEncoded
    @POST("List_RaportUserByGuru/access_token/{access_token}")
    Call<ListRaportResponse> homeraport(@Path("access_token") String token, @Field("id_tutor") String id_user);

    @FormUrlEncoded
    @POST("List_MengajiUserByGuru/access_token/{access_token}")
    Call<ListMengajiResponse> riwayatMengaji(@Path("access_token") String token, @Field("id_tutor") String id_user);

    @FormUrlEncoded
    @POST("RegisterAccountGuru")
    Call<ServerResponse> register(@Field("email") String email,
                                  @Field("password") String password,
                                  @Field("name") String name,
                                  @Field("jenis_kelamin") String jk,
                                  @Field("no_hp") String no_hp,
                                  @Field("usertype") String type);

    @FormUrlEncoded
    @POST("PesertaMengaji")
    Call<ServerResponse> mengaji(@Part("id_user") RequestBody iduser,
                                 @Part("id_tutor") RequestBody idtutor,
                                 @Part("halaman") RequestBody halaman,
                                 @Part("surat") RequestBody surat,
                                 @Part("ayat") RequestBody ayat,
                                 @Part("type") RequestBody type,
                                 @Part MultipartBody.Part image,
                                 @Part MultipartBody.Part sound);
//                                 @PartMap Map<String, RequestBody> map);

    @FormUrlEncoded
    @POST("RaportUser/access_token/{access_token}")
    Call<ServerResponse> nilaiPeserta(@Path("access_token") String token,
                                      @Field("id_user") String iduser,
                                      @Field("id_mengaji") String id_mengaji,
                                      @Field("id_tutor") String idtutor,
                                      @Field("kategori") String kategori,
                                      @Field("jam_belajar") String jam_belajar,
                                      @Field("halaman") String halaman,
                                      @Field("surat") String surat,
                                      @Field("ayat") String ayat,
                                      @Field("nilai_konsistensi") String nilai_konsistensi,
                                      @Field("nilai_ketelitian") String nilai_ketelitian,
                                      @Field("nilai_kerapian") String nilai_kerapian,
                                      @Field("evaluasi") String evaluasi,
                                      @Field("comment") String comment,
                                      @Field("tgl_belajar") String tgl_belajar);

    @FormUrlEncoded
    @POST("Create_ClassFromTutor")
    Call<ServerResponse> createClass(@Field("id_tutor") String idtutor,
                                      @Field("tanggal") String tanggal,
                                      @Field("time") String time,
                                      @Field("durasi") String durasi,
                                      @Field("pelajaran") String pel,
                                      @Field("topikpelajaran") String topikpel,
                                      @Field("user_device") String userdevice,
                                      @Field("user_utc") String utc);
    @FormUrlEncoded
    @POST("List_ClassTutor")
    Call<ListClassResponse> myClass(@Field("id_tutor") String idtutor);

    @GET("List_Guru/access_token/{access_token}")
    Call<ListGuruResponse> getGuru(@Path("access_token") String token);

    @FormUrlEncoded
    @POST("CheckForgotPassword")
    Call<ServerResponse> forgotPassword(@Field("email") String email);

    @FormUrlEncoded
    @POST("resendEmailActivation")
    Call<ServerResponse> resendActivation(@Field("email") String email);

    @FormUrlEncoded
    @POST("profileUser/access_token/{access_token}")
    Call<ProfileResponse> myProfile(@Path("access_token") String token, @Field("id_user") String id_user);

    @FormUrlEncoded
    @POST("setAvailable/access_token/{access_token}")
    Call<ServerResponse> setAvailable(@Path("access_token") String token,
                                   @Field("id_teacher") String id_user,
                                   @Field("status_available") int status_available);

    @FormUrlEncoded
    @POST("updatePhotoProfile/access_token/{access_token}")
    Call<ServerResponse> updatePhotoProfile(@Path("access_token") String token,
                                   @Field("id_user") String id_user,
                                   @Field("image") String image);
    @FormUrlEncoded
    @POST("checkVersion/access_token/{access_token}")
    Call<CheckVersionResponse> checkVersion(@Path("access_token") String token,
                                            @Field("app")String iduser);

    @FormUrlEncoded
    @POST("update_fcm/access_token/{access_token}")
    Call<ServerResponse> sendFCM(@Field("id_user") String id_user,
                                 @Field("fcm_token") String fcm_token,
                                 @Path("access_token") String token);

    @FormUrlEncoded
    @POST("List_Peserta_byGuru/access_token/{access_token}")
    Call<ListPesertaResponse> getMyStudent(@Path("access_token") String token,
                                           @Field("id_teacher") String id_teacher);

    @FormUrlEncoded
    @POST("List_Peserta_byFollowGuru/access_token/{access_token}")
    Call<ListPesertaResponse> getMyStudentNew(@Path("access_token") String token,
                                           @Field("id_teacher") String id_teacher);

    @FormUrlEncoded
    @POST("Profile_ChangePassword/access_token/{access_token}")
    Call<ServerResponse> setPassword(@Path("access_token") String token,
                                     @Field("id") String iduser,
                                     @Field("old_pass") String old_pass,
                                     @Field("new_pass") String new_pass);

    @GET("ListTopUpload_Mengaji/access_token/{access_token}/data/{jum}")
    Call<ListTopMengaji> getTopMengaji(@Path("access_token") String token,
                                       @Path("jum") String sJumData);


    // CHAT

    @FormUrlEncoded
    @POST("user/create")
    Call<UserCreateResponse> chatCreate(@Field("id")String iduser,
                                        @Field("first_name")String fname,
                                        @Field("last_name")String lname);

    @FormUrlEncoded
    @POST("user/login")
    Call<ChatLoginResponse> chatLogin(@Field("id")String iduser);

    @FormUrlEncoded
    @POST("group/create")
    Call<ChatGroupCreateResponse> chatCreateGroup(@Field("name")String name, @Field("id")String id);

    @FormUrlEncoded
    @POST("group/add_user")
    Call<ChatAddUserResponse> chatAddtoGroup(@Field("id_group")String id_group, @Field("id_user")String id);

    @FormUrlEncoded
    @POST("group/send")
    Call<ChatSendMsgResp> chatSendMsg(@Field("id_group")String id_group, @Field("id_user")String id,
                                      @Field("message")String msg);

    @FormUrlEncoded
    @POST("group/receive")
    Call<ChatReceiveMsg> chatReceiveMsg(@Field("id_group")String id_group);

    @FormUrlEncoded
    @POST("group/unread")
    Call<ChatUnreadMsg> chatUnreadMsg(@Field("name_group")String name_group,
                                          @Field("id_user")String id_user);

    @FormUrlEncoded
    @POST("group/read")
    Call<ChatSendMsgResp> chatReadMsg(@Field("id_user") String id_user, @Field("id_group") String id_group);
}