package com.apps.majiguru;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.DataProfile;
import com.apps.majiguru.Model.ProfileResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    private static final String TAG = "ProfileActivity";
    FloatingActionButton fab;
    CircleImageView image, images;
    SessionManager sessionManager;
    DataProfile dataProfile;
    TextView tvnama, tvemail, tvphone;
    String sToken, idUser;
    Data myData;
    Uri resultUri;
    public ApiConfig restApi;
    android.app.AlertDialog b;
    android.app.AlertDialog.Builder dialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        sessionManager = new SessionManager(this);
        restApi = AppConfig.getRetrofit().create(ApiConfig.class);

        String dt = sessionManager.getData();
        myData = Data.create(dt);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
        Log.i(TAG, "onCreate: "+myData.toString());
        Log.i(TAG, "onCreate: "+idUser);

        initView();
        initToolbar();
        initComponent();
        initOnclick();

        String d = sessionManager.getDataProfile();
        if (d != null) {
            dataProfile = DataProfile.create(d);
            int im;
            if (dataProfile.getJk().equals("L"))
                im = R.drawable.icon_man;
            else
                im = R.drawable.icon_girl;
            Glide.with(this)
                    .load(dataProfile.getUserImage())
                    .apply(new RequestOptions().placeholder(im))
                    .into(image);
            tvnama.setText(dataProfile.getName());
            tvemail.setText(dataProfile.getEmail());
            tvphone.setText(dataProfile.getNoHp());
        }
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Profil");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initComponent() {
        image = findViewById(R.id.image);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ProfileActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.fullimg);
                dialog.setCancelable(true);
                dialog.show();
                PhotoView zoom = dialog.findViewById(R.id.photo_view);
                zoom.setImageDrawable(image.getDrawable());
                zoom.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
            }
        });

        final CollapsingToolbarLayout collapsing_toolbar = findViewById(R.id.collapsing_toolbar);
        ((AppBarLayout) findViewById(R.id.app_bar_layout)).addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int min_height = ViewCompat.getMinimumHeight(collapsing_toolbar) * 2;
                float scale = (float) (min_height + verticalOffset) / min_height;
                image.setScaleX(scale >= 0 ? scale : 0);
                image.setScaleY(scale >= 0 ? scale : 0);
                if (scale >= 0){
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0, Method.dpToPx((int) (scale+60), ProfileActivity.this),0, Method.dpToPx(20, ProfileActivity.this));
                    tvnama.setLayoutParams(params);
                } else {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    params.setMargins(0,Method.dpToPx(20, ProfileActivity.this),0, Method.dpToPx(20, ProfileActivity.this));
                    tvnama.setLayoutParams(params);
                }
            }
        });
    }

    void initView() {
        fab = findViewById(R.id.fab);
        tvnama = findViewById(R.id.tvNama);
        tvemail = findViewById(R.id.tvEmail);
        tvphone = findViewById(R.id.tvPhone);
        images = findViewById(R.id.images);
        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(ProfileActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.fullimg);
                dialog.setCancelable(true);
                dialog.show();
                PhotoView zoom = dialog.findViewById(R.id.photo_view);
                zoom.setImageDrawable(images.getDrawable());
                zoom.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
            }
        });
    }

    void initOnclick() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setCropMenuCropButtonTitle("Done")
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .setMultiTouchEnabled(false)
                        .setRequestedSize(512, 512)
                        .setAspectRatio(1, 1)
                        .start(ProfileActivity.this);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                images.setImageURI(resultUri);
                BitmapDrawable drawable = (BitmapDrawable) images.getDrawable();

                Bitmap myBitmap;
                myBitmap = drawable.getBitmap();

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                byte[] ba = bytes.toByteArray();
                String b64 = Base64.encodeToString(ba, Base64.NO_WRAP);
                ShowProgressDialog();
                updateProfileImage(b64);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    void updateProfileImage(String b64){
        restApi.updatePhotoProfile(sToken, idUser, b64).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());
                    if (response.body().isSuccess()) {
                        Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        image.setImageURI(resultUri);
                    } else
                        Toast.makeText(ProfileActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
                HideProgressDialog();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(ProfileActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
                HideProgressDialog();
            }
        });
    }


    public void ShowProgressDialog() {
        dialogBuilder = new android.app.AlertDialog.Builder(ProfileActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View dialogView = inflater.inflate(R.layout.progress_dialog_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setCancelable(false);
        b = dialogBuilder.create();
        b.show();
    }

    public void HideProgressDialog() {
        b.dismiss();
    }
}
