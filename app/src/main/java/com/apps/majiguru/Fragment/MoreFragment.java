package com.apps.majiguru.Fragment;


import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.AboutActivity;
import com.apps.majiguru.HomeActivity;
import com.apps.majiguru.Model.DataProfile;
import com.apps.majiguru.Model.Datum;
import com.apps.majiguru.Model.ListPesertaResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.PasswordActivity;
import com.apps.majiguru.ProfileActivity;
import com.apps.majiguru.R;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoreFragment extends Fragment {

    public static final int DIALOG_QUEST_CODE = 300;
    private static final String TAG = "MoreFragment";
    TextView tvStatus, tvVersion, tvSum, tvnama, tvemail;
    ImageView img;
    SessionManager sessionManager;
    HomeActivity homeActivity;
    View btnLogout, llProfile, llStatus, llStudent, llAbout, llNilai, llPass;
    DataProfile dataProfile;
    AlertDialog.Builder builder;
    List<Datum> datumList = new ArrayList<>();
    int back = 0;

    private static final String[] MyStatus = new String[]{
            "Tidak Aktif", "Aktif"
    };

    int selectAvailable = 0;

    public MoreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_more, container, false);
        homeActivity = (HomeActivity) getActivity();

        sessionManager = new SessionManager(getContext());

        initView(v);
        initClick();
        initProfile();
        getMyStudent();

        return v;
    }

    void initView(View v) {

        img = v.findViewById(R.id.imgMy);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.fullimg);
                dialog.setCancelable(true);
                dialog.show();
                PhotoView zoom = dialog.findViewById(R.id.photo_view);
                zoom.setImageDrawable(img.getDrawable());
                zoom.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
            }
        });
        tvnama = v.findViewById(R.id.tvNama);
        tvemail = v.findViewById(R.id.tvEmail);
        llProfile = v.findViewById(R.id.tvUbah);
        btnLogout = v.findViewById(R.id.llLogouts);
        llStatus = v.findViewById(R.id.llMystatus);

        tvStatus = v.findViewById(R.id.tvmystatus);
        tvSum = v.findViewById(R.id.tvSumStudent);
        tvVersion = v.findViewById(R.id.tvVersion);
        tvVersion.setText("v" + Method.checkVersion(homeActivity));

        llStudent = v.findViewById(R.id.llStudent);
        llStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogFullscreen();
                back = 1;
            }
        });

        llPass = v.findViewById(R.id.llPass);
        llAbout = v.findViewById(R.id.llAbout);
        llNilai = v.findViewById(R.id.llNilai);
    }

    public void doBack() {
        if (back == 1){
            back = 0;
        } else
            homeActivity.doMenuFirst();

    }

    void initClick() {

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog("Apakah Anda ingin keluar dari akun ini?");
            }
        });

        llProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeActivity.startActivity(new Intent(homeActivity, ProfileActivity.class));
            }
        });

        llStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSingleChoiceDialog();
            }
        });

        llPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, PasswordActivity.class));
            }
        });

        llAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(homeActivity, AboutActivity.class));
            }
        });

        llNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String appPackageName = homeActivity.getPackageName(); // getPackageName() from Context or Activity object
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });
    }

    public void showSingleChoiceDialog() {
        if (dataProfile != null)
            selectAvailable = Integer.parseInt(dataProfile.getStatusAvailable());
        builder = new AlertDialog.Builder(homeActivity);
        builder.setTitle("Tetapkan Status Saya");
        builder.setSingleChoiceItems(MyStatus, selectAvailable, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectAvailable = i;
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setAvailable(selectAvailable);
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    public void initProfile() {
        String d = sessionManager.getDataProfile();
        if (d != null) {
            dataProfile = DataProfile.create(d);
            tvnama.setText(dataProfile.getName());
            tvemail.setText(dataProfile.getEmail());
            int im;
            if (dataProfile.getJk().equals("L"))
                im = R.drawable.icon_man;
            else
                im = R.drawable.icon_girl;
            Glide.with(homeActivity)
                    .load(dataProfile.getUserImage())
                    .apply(new RequestOptions().placeholder(im))
                    .into(img);

            Log.w(TAG, "initProfile: " + dataProfile.getStatusAvailable());
            Log.e(TAG, "initProfile: " + MyStatus[Integer.parseInt(dataProfile.getStatusAvailable())]);
            if (dataProfile.getStatusAvailable().equals("1")) {
                tvStatus.setText(MyStatus[Integer.parseInt(dataProfile.getStatusAvailable())]);
                tvStatus.setTextColor(Color.parseColor("#388E3C"));
            } else {
                tvStatus.setText(MyStatus[Integer.parseInt(dataProfile.getStatusAvailable())]);
                tvStatus.setTextColor(Color.parseColor("#D32F2F"));
            }
        }
    }

    void setAvailable(int a) {
        homeActivity.restApi.setAvailable(homeActivity.sToken, homeActivity.idUser, a).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());
                    if (response.body().isSuccess()) {
                        Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        homeActivity.getMyProfile();
                    } else
                        Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void updateFCM() {
        homeActivity.restApi.sendFCM(homeActivity.idUser, "kosong", sessionManager.getToken()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }

                sessionManager.logoutUser();
                homeActivity.finish();
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: updateFCM " + t.getMessage());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getMyStudent() {
        homeActivity.restApi.getMyStudentNew(homeActivity.sToken, homeActivity.idUser).enqueue(new Callback<ListPesertaResponse>() {
            @Override
            public void onResponse(Call<ListPesertaResponse> call, Response<ListPesertaResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getData() != null) {
                            if (response.body().getData().size() > 0) {
                                tvSum.setText(response.body().getData().size() + " Siswa");
                                datumList = response.body().getData();
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListPesertaResponse> call, Throwable t) {

                Log.e(TAG, "onFailure:getMyStudent " + t.getMessage());
            }
        });
    }

    private void showDialogFullscreen() {
        FragmentManager fragmentManager = homeActivity.getSupportFragmentManager();
        ListStudentFragment newFragment = new ListStudentFragment();
        newFragment.setRequestCode(DIALOG_QUEST_CODE);
        newFragment.setDatum(datumList);
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.add(android.R.id.content, newFragment).addToBackStack(null).commit();
    }

    public void showDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(homeActivity);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("IYA", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                updateFCM();
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
