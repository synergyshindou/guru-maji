package com.apps.majiguru.Fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.apps.majiguru.Adapter.ListStudentAdapter;
import com.apps.majiguru.Model.Datum;
import com.apps.majiguru.R;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListStudentFragment extends DialogFragment {

    private static final String TAG = "ListStudentFragment";
    private int request_code = 0;
    private View root_view;
    List<Datum> datumList;
    RecyclerView recyclerView;
    TextView tvNo;

    public ListStudentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root_view = inflater.inflate(R.layout.fragment_list_student, container, false);
        recyclerView = root_view.findViewById(R.id.rvStudent);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        ListStudentAdapter listStudentAdapter = new ListStudentAdapter(getContext(), datumList, new ListStudentAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Datum item) {

            }
        });
        recyclerView.setAdapter(listStudentAdapter);
        ((ImageButton) root_view.findViewById(R.id.bt_close)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvNo = root_view.findViewById(R.id.tvnodata);
        if (datumList.size() == 0)
            tvNo.setVisibility(View.VISIBLE);
        return root_view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                //do your stuff
                dismiss();
            }
        };
    }

    public void setRequestCode(int request_code) {
        this.request_code = request_code;
    }

    public void setDatum(List<Datum> list) {
        this.datumList = list;
    }
}
