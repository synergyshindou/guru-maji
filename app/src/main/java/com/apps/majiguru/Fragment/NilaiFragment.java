package com.apps.majiguru.Fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.Adapter.MengajiAdapter;
import com.apps.majiguru.HomeActivity;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.DataMengaji;
import com.apps.majiguru.Model.ListMengajiResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.R;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.apps.majiguru.Widget.CustomRangeInputFilter;
import com.apps.majiguru.Widget.SpacesItemDecoration;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN;

/**
 * A simple {@link Fragment} subclass.
 */
public class NilaiFragment extends Fragment {


    private static final String TAG = "NilaiFragment";
    SessionManager sessionManager;
    Data myData;
    ApiConfig getResponse;
    String sToken, idUser, audioUrl, sGuruID = "";
    View llMain, llNilai;
    ImageView imgNilai;
    RecyclerView recyclerView;
    MengajiAdapter mengajiAdapter;
    List<DataMengaji> dataMengajis = new ArrayList<>();
    DataMengaji dataMengaji;
    ProgressBar pb;
    TextView tvNo, tvNama, tvHalaman, tvNoData;
    EditText etKera, etKons, etKete;
    Button btnSave;
    int back = 0;
    HomeActivity homeActivity;
    private Button mButtonPlay;
    private Button mButtonStop;
    private SeekBar mSeekBar;
    private TextView mPass;
    private TextView mDuration;
    private TextView mDue;
    private MediaPlayer mPlayer;
    private Handler mHandler;
    private Runnable mRunnable;
    private boolean initialStage = true;
    private boolean playPause;
    private ProgressDialog progressDialog;
    List<String> list = new ArrayList<>();
    MaterialSpinner spinGur;
    TextInputEditText etComment;

    public NilaiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
        Log.i(TAG, "onCreate: " + sToken);
        Log.i(TAG, "onCreate: " + idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_nilai, container, false);
        homeActivity = (HomeActivity) getActivity();
        homeActivity.getWindow().setSoftInputMode(SOFT_INPUT_ADJUST_PAN);
        initView(v);

        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

        progressDialog = new ProgressDialog(getContext());
        getRaport(true);

        return v;
    }

    void initView(View v) {

        llMain = v.findViewById(R.id.llMain);
        llNilai = v.findViewById(R.id.llNilai);

        imgNilai = v.findViewById(R.id.imgNilai);

        tvNama = v.findViewById(R.id.tvNama);
        tvHalaman = v.findViewById(R.id.tvHalaman);

        etKera = v.findViewById(R.id.etKera);
        etKons = v.findViewById(R.id.etKons);
        etKete = v.findViewById(R.id.etKete);

        etKera.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, 10)});
        etKons.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, 10)});
        etKete.setFilters(new InputFilter[]{new CustomRangeInputFilter(0, 10)});

        spinGur = v.findViewById(R.id.spinGur);

        list = new ArrayList<>();
        list.add("Alhamdulillah Suaranya Merdu dan Syahdu");
        list.add("Alhamdulillah Suaranya Tegas dan Jelas");
        list.add("Menakjubkan Tulisannya Rapi");
        list.add("Menakjubkan Tulisannya Indah");
        list.add("Menakjubkan Tulisannya Jelas");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(homeActivity, android.R.layout.simple_spinner_item, list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinGur.setAdapter(adapter);
        spinGur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                Log.d(TAG, selectedItemText);
                if (position >= 0) {
                    spinGur.setError(null);
                    spinGur.setEnableErrorLabel(false);
                    sGuruID = list.get(position);
                } else {
                    sGuruID = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etComment = v.findViewById(R.id.etCatatan);

        tvNoData = v.findViewById(R.id.tvnodata);
        tvNo = v.findViewById(R.id.tvnoconnect);
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRaport(true);
            }
        });

        pb = v.findViewById(R.id.pb_load);
        recyclerView = v.findViewById(R.id.rvNilai);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        // Get the widget reference from xml layout
        btnSave = v.findViewById(R.id.btn_nilai);
        mButtonPlay = v.findViewById(R.id.btn_playa);
        mButtonStop = v.findViewById(R.id.btn_stopa);
        mSeekBar = v.findViewById(R.id.seek_bar);
        mPass = v.findViewById(R.id.tv_pass);
        mDuration = v.findViewById(R.id.tv_duration);
        mDue = v.findViewById(R.id.tv_due);

        // Initialize the handler
        mHandler = new Handler();

        // Click listener for playing button
        mButtonPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (initialStage) {
                    initMP();
                    new Player().execute(audioUrl);
                } else {
                    if (!mPlayer.isPlaying())
                        mPlayer.start();
                }

            }
        });

        // Set a click listener for top playing button
        mButtonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPlaying();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etKons.getText().length() == 0) {
                    etKons.setError("Mohon diisi");
                    etKons.requestFocus();
                } else if (etKete.getText().length() == 0) {
                    etKete.setError("Mohon diisi");
                    etKete.requestFocus();
                } else if (etKera.getText().length() == 0) {
                    etKera.setError("Mohon diisi");
                    etKera.requestFocus();
                } else if (sGuruID.equals("")) {
                    spinGur.setError("Mohon dipilih");
                    spinGur.setEnableErrorLabel(true);
                } else if (etComment.getText().length() == 0) {
                    etComment.setError("Mohon diisi");
                    etComment.requestFocus();
                } else
                    saveNilai();
            }
        });

        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (mPlayer != null && b) {
                    Log.w(TAG, "onProgressChanged: " + i);
                    mPlayer.seekTo(i * 1000);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        imgNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getContext());
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(
                        new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.setContentView(R.layout.fullimg);
                dialog.setCancelable(true);
                dialog.show();
                PhotoView zoom = dialog.findViewById(R.id.photo_view);
                zoom.setImageDrawable(imgNilai.getDrawable());
                zoom.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        // TODO Auto-generated method stub
                        dialog.dismiss();

                    }
                });
            }
        });
    }

    void initMP() {
        mPlayer = new MediaPlayer();
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlaying();
            }

        });
        mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                // Initialize the seek bar
                initializeSeekBar();
                // Get the current audio stats
                mSeekBar.setProgress(0);
                // Start the media player
                mPlayer.start();

            }
        });

    }

    public void getRaport(boolean isFirst) {
        tvNo.setVisibility(View.GONE);
        if (isFirst)
            showProgress(true);
        getResponse.riwayatMengaji(sToken, idUser).enqueue(new Callback<ListMengajiResponse>() {
            @Override
            public void onResponse(Call<ListMengajiResponse> call, Response<ListMengajiResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                showProgress(false);
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());

                    if (response.body().isStatus()) {
                        dataMengajis = response.body().getDataMengaji();

                        if (dataMengajis != null && dataMengajis.size() > 0) {
                            Collections.sort(dataMengajis, DataMengaji.DateNameComparator);
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(horizontalLayoutManagaer);
                            mengajiAdapter = new MengajiAdapter(getContext(), dataMengajis, new MengajiAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(DataMengaji item) {
                                    dataMengaji = item;
                                    llMain.setVisibility(View.GONE);
                                    llNilai.setVisibility(View.VISIBLE);
                                    back = 1;
                                    audioUrl = item.getSound();
                                    homeActivity.tvTitle.setText("Penilaian Peserta");
                                    tvNama.setText("Nama Peserta : " + item.getName());
                                    tvHalaman.setText(item.getHalaman() + ", " + item.getSurat() + " - Ayat " + item.getAyat());

                                    Glide.with(getActivity())
                                            .load(item.getImage())
//                                            .apply(RequestOptions.circleCropTransform())
                                            .into(imgNilai);
                                    Log.i(TAG, "onItemClick: Audio " + audioUrl);
                                }
                            });

                            recyclerView.setAdapter(mengajiAdapter);
                            tvNoData.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        tvNoData.setVisibility(View.VISIBLE);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    tvNoData.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ListMengajiResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                tvNo.setVisibility(View.VISIBLE);
                showProgress(false);
            }
        });
    }

    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public void doBack() {
        if (back == 1) {

            llMain.setVisibility(View.VISIBLE);
            llNilai.setVisibility(View.GONE);
            homeActivity.tvTitle.setText("Nilai");
            back = 0;
            etKera.setText("");
            etKons.setText("");
            etKete.setText("");
            spinGur.setSelection(0);
            etComment.setText("");
        } else {
            homeActivity.doMenuFirst();
        }
    }

    protected void stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
            mPlayer = null;
//            Toast.makeText(getContext(),"Stop playing.",Toast.LENGTH_SHORT).show();
            if (mHandler != null) {
                mHandler.removeCallbacks(mRunnable);
            }
            mSeekBar.setProgress(0);
            initialStage = true;
        }
    }

    protected void initializeSeekBar() {
        mSeekBar.setMax(mPlayer.getDuration() / 100);

        mRunnable = new Runnable() {
            @Override
            public void run() {
                if (mPlayer != null) {
                    int mCurrentPosition = mPlayer.getCurrentPosition() / 100; // In milliseconds
                    mSeekBar.setProgress(mCurrentPosition);
                }
                mHandler.postDelayed(mRunnable, 100);
            }
        };
        mHandler.postDelayed(mRunnable, 100);
    }

    public void saveNilai() {
        getResponse.nilaiPeserta(sToken, dataMengaji.getIdUser(), dataMengaji.getIdMengaji(), dataMengaji.getIdTutor(),
                dataMengaji.getType(), Method.FormatJamOnly(dataMengaji.getCreatedAt()), dataMengaji.getHalaman(),
                dataMengaji.getSurat(), dataMengaji.getAyat(), etKons.getText().toString().trim(),
                etKete.getText().toString().trim(), etKera.getText().toString().trim(), sGuruID, etComment.getText().toString(),
                Method.FormatDateSQL(dataMengaji.getCreatedAt()))
                .enqueue(new Callback<ServerResponse>() {
                    @Override
                    public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                        Log.i(TAG, "onResponse: " + response.message());
                        if (response.body() != null) {
                            Log.i(TAG, "onResponse: " + response.body());
                            if (response.body().isSuccess()) {
                                Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                getRaport(false);
                                doBack();
                            } else
                                Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<ServerResponse> call, Throwable t) {
                        Log.e(TAG, "message = " + t.getMessage());
                        Log.e(TAG, "cause = " + t.getCause());
                        Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    class Player extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            Boolean prepared = false;

            try {
                mPlayer.setDataSource(strings[0]);
                mPlayer.prepare();
                prepared = true;

            } catch (Exception e) {
                Log.e("MyAudioStreamingApp", e.getMessage());
                prepared = false;
            }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            if (progressDialog.isShowing()) {
                progressDialog.cancel();
            }

            mPlayer.start();
            initialStage = false;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("Buffering...");
            progressDialog.show();
        }
    }

}
