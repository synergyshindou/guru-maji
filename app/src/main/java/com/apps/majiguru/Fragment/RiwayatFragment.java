package com.apps.majiguru.Fragment;


import android.app.ProgressDialog;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.apps.majiguru.Adapter.MengajiAdapter;
import com.apps.majiguru.Adapter.MyHomeAdapter;
import com.apps.majiguru.HomeActivity;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.DataMengaji;
import com.apps.majiguru.Model.Dataraport;
import com.apps.majiguru.Model.ListMengajiResponse;
import com.apps.majiguru.Model.ListRaportResponse;
import com.apps.majiguru.R;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.apps.majiguru.Widget.SpacesItemDecoration;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiwayatFragment extends Fragment {

    public RiwayatFragment() {
        // Required empty public constructor
    }

    int back = 0;
    private static final String TAG = "RiwayatFragment";
    SessionManager sessionManager;

    Data myData;

    ApiConfig getResponse;

    String sToken, idUser;

    RecyclerView recyclerView;
    MyHomeAdapter myHomeAdapter;
    List<Dataraport> dataraports = new ArrayList<>();
    ProgressBar pb;
    TextView tvNo;
    View  llNo;
    HomeActivity homeActivity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_riwayat, container, false);
        homeActivity = (HomeActivity) getActivity();
        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);
        initView(v);
        getRaport(true);
        return v;
    }

    void initView(View v) {

        tvNo = v.findViewById(R.id.tvnoconnect);
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRaport(true);
            }
        });

        pb = v.findViewById(R.id.pb_load);
        recyclerView = v.findViewById(R.id.rvRiwayat);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        llNo = v.findViewById(R.id.llNo);

    }

    public void getRaport(boolean isFirst){
        tvNo.setVisibility(View.GONE);
        if (isFirst)
            showProgress(true);
        getResponse.homeraport(sToken, idUser).enqueue(new Callback<ListRaportResponse>() {
            @Override
            public void onResponse(Call<ListRaportResponse> call, Response<ListRaportResponse> response) {
                Log.i(TAG, "onResponse: "+response.message());
                showProgress(false);
                if (response.body() != null){
                    Log.i(TAG, "onResponse: "+response.body());

                    if (response.body().isStatus()) {
                        dataraports = response.body().getDataraport();
                        if (dataraports != null && dataraports.size() > 0) {
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            recyclerView.setLayoutManager(horizontalLayoutManagaer);
                            myHomeAdapter = new MyHomeAdapter(getContext(), dataraports, new MyHomeAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(Dataraport item) {
                                    Log.i(TAG, "onItemClick: " + item.toString());
                                }
                            });
                            recyclerView.setAdapter(myHomeAdapter);
                            llNo.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            llNo.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        llNo.setVisibility(View.VISIBLE);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    llNo.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ListRaportResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                tvNo.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                showProgress(false);
            }
        });
    }
    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
//        llDet.setVisibility(View.GONE);
    }


    public void doBack() {
        if (back == 1) {
//            llDet.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            back = 0;
            homeActivity.tvTitle.setText("Riwayat");
        } else {
            homeActivity.doMenuFirst();
        }
    }
}
