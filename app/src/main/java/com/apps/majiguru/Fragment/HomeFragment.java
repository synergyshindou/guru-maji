package com.apps.majiguru.Fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.apps.majiguru.Adapter.MyHomeAdapter;
import com.apps.majiguru.Adapter.MyHomeUploadAdapter;
import com.apps.majiguru.HomeActivity;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.DataTopMengaji;
import com.apps.majiguru.Model.Dataraport;
import com.apps.majiguru.Model.ListRaportResponse;
import com.apps.majiguru.Model.ListTopMengaji;
import com.apps.majiguru.R;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.apps.majiguru.Widget.SpacesItemDecoration;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okio.BufferedSink;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static kotlin.io.ConstantsKt.DEFAULT_BUFFER_SIZE;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    private static final String TAG = "HomeFragment";
    TextView tvNo, tvNoData;
    SessionManager sessionManager;

    Data myData;

    ApiConfig getResponse;

    String sToken, idUser;

    RecyclerView rvUpload;
    MyHomeUploadAdapter myHomeUploadAdapter;
    List<DataTopMengaji> dataTopMengajis= new ArrayList<>();

    ProgressBar pb;
    HomeActivity homeActivity;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
        Log.i(TAG, "onCreate: "+sToken);
        Log.i(TAG, "onCreate: "+idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);
        initView(v);

        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

        getRaport(true);
        return v;
    }

    void initView(View v){

        tvNoData = v.findViewById(R.id.tvnodata);
        tvNo = v.findViewById(R.id.tvnoconnect);
        tvNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRaport(true);
            }
        });

        pb = v.findViewById(R.id.pb_load);

        rvUpload = v.findViewById(R.id.rvHome);
        rvUpload.addItemDecoration(new SpacesItemDecoration(20));

    }

    public void getRaport(boolean isFirst){
        tvNo.setVisibility(View.GONE);
        if (isFirst)
            showProgress(true);
        getResponse.getTopMengaji(sToken, "5").enqueue(new Callback<ListTopMengaji>() {
            @Override
            public void onResponse(Call<ListTopMengaji> call, Response<ListTopMengaji> response) {
                Log.i(TAG, "onResponse: "+response.message());
                showProgress(false);
                if (response.body() != null){
                    Log.i(TAG, "onResponse: "+response.body().toString());

                    if (response.body().isStatus()) {
                        dataTopMengajis = response.body().getDataTopMengaji();

                        if (dataTopMengajis != null && dataTopMengajis.size() > 0) {
                            LinearLayoutManager horizontalLayoutManagaer
                                    = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
                            rvUpload.setLayoutManager(horizontalLayoutManagaer);
                            myHomeUploadAdapter = new MyHomeUploadAdapter(getContext(), dataTopMengajis, new MyHomeUploadAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(DataTopMengaji item) {
                                    Log.i(TAG, "onItemClick: " + item.toString());
                                }
                            });
                            rvUpload.setAdapter(myHomeUploadAdapter);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ListTopMengaji> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                tvNo.setVisibility(View.VISIBLE);
                showProgress(false);
            }
        });
    }

    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        rvUpload.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
