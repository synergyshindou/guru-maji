package com.apps.majiguru.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.Adapter.KelasAdapter;
import com.apps.majiguru.Classroom;
import com.apps.majiguru.HomeActivity;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.ListClas;
import com.apps.majiguru.Model.ListClassResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.R;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.Method;
import com.apps.majiguru.Utils.SessionManager;
import com.apps.majiguru.Widget.MaterialSpinner;
import com.apps.majiguru.Widget.SpacesItemDecoration;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class MengajiFragment extends Fragment implements TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {

    private static final String TAG = "MengajiFragment";
    private static final SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat FORMATTERINDO = new SimpleDateFormat("dd MMMM yyyy");
    public String sfdate, sftimehours;
    SimpleDateFormat sdfdate = new SimpleDateFormat("EEEE, dd MMMM yyyy");
    SimpleDateFormat sdftimehour = new SimpleDateFormat("HH");
    SimpleDateFormat sdftimeminute = new SimpleDateFormat("mm");
    String sToken, ECTgl = "0", ECTime = "0", idUser, cfDuration = "0";
    Calendar now;
    RecyclerView recyclerView;
    ProgressBar pb;
    private Calendar cal;
    TimePickerDialog tpd;
    DatePickerDialog dpd;
    EditText tilDate, tilTime, tilTopic;
    View v, llMain, llCClass, llNoItem;
    FloatingActionButton fab;
    int  iJamPlus, iMenit = 1, back = 0;
    Button btnSave;
    TextView tvRun;

    SessionManager sessionManager;
    Data myData;
    ApiConfig getResponse;

    MaterialSpinner spinner;
    HomeActivity homeActivity;
    String[]  sDuration, sDurationValue;
    KelasAdapter kelasAdapter;
    List<ListClas> listClas;

    public MengajiFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getContext());
        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();
        Log.i(TAG, "onCreate: " + sToken);
        Log.i(TAG, "onCreate: " + idUser);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_mengaji, container, false);
        homeActivity = (HomeActivity) getActivity();

        sDuration = getResources().getStringArray(R.array.duration_name);
        sDurationValue = getResources().getStringArray(R.array.duration_value);
        initView(v);

        getResponse = AppConfig.getRetrofit().create(ApiConfig.class);

        getMyClass();
        return v;
    }

    public void getMyClass(){

        showProgress(true);
        getResponse.myClass(homeActivity.idUser).enqueue(new Callback<ListClassResponse>() {
            @Override
            public void onResponse(Call<ListClassResponse> call, Response<ListClassResponse> response) {
                showProgress(false);
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null){
                    if (response.body().isStatus()){
                        if (response.body().getListClass().size() > 0){
                            listClas = response.body().getListClass();
                            kelasAdapter = new KelasAdapter(homeActivity, listClas, new KelasAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(ListClas item) {
                                    Log.i(TAG, "onItemClick: "+item.toString());
                                }

                                @Override
                                public void onItemClick(ListClas item, String s) {
                                    if (s.equals("enter")){
                                        Intent in = new Intent(homeActivity, Classroom.class);
                                        in.putExtra("id_user", homeActivity.idUser);
                                        in.putExtra("class_id", item.getClassId());
                                        startActivity(in);
                                    }
                                }
                            });

                            recyclerView.setAdapter(kelasAdapter);
                            recyclerView.setVisibility(View.VISIBLE);
                            llNoItem.setVisibility(View.GONE);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            llNoItem.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        llNoItem.setVisibility(View.VISIBLE);
                    }
                } else {
                    recyclerView.setVisibility(View.GONE);
                    llNoItem.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<ListClassResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
                showProgress(false);
            }
        });
    }

    public void doCreateClass(){
        getResponse.createClass(idUser, ECTgl, ECTime+":00", cfDuration,"795", tilTopic.getText().toString().trim(),
                "mobile", Method.getUTCTime()).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());
                    if (response.body().isSuccess()) {
                        Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getMyClass();
                        doBack();
                    } else
                        Toast.makeText(homeActivity, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(homeActivity, "Please try again. . .", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void initView(View sv){

        llMain = sv.findViewById(R.id.llMain);
        llNoItem = sv.findViewById(R.id.llNoItem);
        llCClass = sv.findViewById(R.id.llCreateClass);

        tvRun = v.findViewById(R.id.tvTkno);
        tvRun.setSelected(true);

        pb = v.findViewById(R.id.pb_load);
        recyclerView = v.findViewById(R.id.rvHome);
        recyclerView.addItemDecoration(new SpacesItemDecoration(20));

        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);

        fab = sv.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llMain.setVisibility(View.GONE);
                llCClass.setVisibility(View.VISIBLE);
                back = 1;
            }
        });

        spinner = sv.findViewById(R.id.spingrade);
        ArrayAdapter<String> adapterc = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, sDuration);
        adapterc.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapterc);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                Log.d("LEVEL", selectedItemText);
                if (position >= 0) {
                    spinner.setError(null);
                    cfDuration = sDurationValue[position];
                } else {
                    cfDuration = "0";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tilDate = sv.findViewById(R.id.etdate);

        tilTime = sv.findViewById(R.id.ettime);

        tilTopic = sv.findViewById(R.id.ettopic);

        cal = Calendar.getInstance();
        sfdate = sdfdate.format(cal.getTime());
        tilDate.setText(FORMATTERINDO.format(cal.getTime()));

        sftimehours = sdftimehour.format(cal.getTime());
        iJamPlus = Integer.parseInt(sftimehours) + 1;

        sftimehours = sdftimeminute.format(cal.getTime());
        iMenit = Integer.parseInt(sftimehours);
        if (iMenit >= 0 && iMenit < 15)
            iMenit = 15;
        else if (iMenit > 15 && iMenit < 30)
            iMenit = 30;
        else if (iMenit > 30 && iMenit < 45)
            iMenit = 45;

        ECTgl = FORMATTER.format(cal.getTime());

        if (iMenit > 45) {
            tilTime.setText(String.valueOf(iJamPlus + 1) + ":00");
            ECTime = String.valueOf(iJamPlus + 1) + ":00";
        } else {
            tilTime.setText(String.valueOf(iJamPlus) + ":" + iMenit);
            ECTime = String.valueOf(iJamPlus) + ":" + iMenit;
        }
        setDateTimeField();

        btnSave = sv.findViewById(R.id.btncreateclass);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                String stopic = tilTopic.getText().toString();
                if (cfDuration.equalsIgnoreCase("0"))
                    spinner.setError("Please Select");
                else if (stopic.isEmpty()) {
                    tilTopic.setError("Cannot be Empty");
                    tilTopic.requestFocus();
                } else {
                    doCreateClass();
                }
            }
        });
    }


    public void hideSoftKeyboard() {
        if (homeActivity.getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) homeActivity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(homeActivity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void ShowDateFree(View view) {
        dpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    public void ShowTimeFree(View v) {
        tpd.show(getActivity().getFragmentManager(), "Datepickerdialog");
    }

    private void setDateTimeField() {

        boolean is24 = DateFormat.is24HourFormat(getContext());
        now = Calendar.getInstance();
        // Datepickerdialog
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );

        dpd.setVersion(DatePickerDialog.Version.VERSION_2);
        dpd.setAccentColor("#009688");
        dpd.setMinDate(Calendar.getInstance());

        // Timepickerdialog
        tpd = TimePickerDialog.newInstance(
                this,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                is24
        );

        tpd.setAccentColor("#009688");

        sftimehours = sdftimehour.format(cal.getTime());
        iJamPlus = Integer.parseInt(sftimehours) + 1;
        Timepoint time;

        if (iMenit > 45)
            time = new Timepoint(iJamPlus + 1);
        else
            time = new Timepoint(iJamPlus, iMenit);

        String date = sdfdate.format(cal.getTime());
        if (sfdate.equalsIgnoreCase(date)) {
            tpd.setMinTime(time);
        }

        tpd.setTimeInterval(1, 15);
        tpd.enableMinutes(true);

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        now.set(year, monthOfYear, dayOfMonth);
        sfdate = sdfdate.format(now.getTime());
        ECTgl = FORMATTER.format(now.getTime());
        tilDate.setText(FORMATTERINDO.format(now.getTime()));
        setDateTimeField();

    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String hourString = hourOfDay < 10 ? "0" + hourOfDay : "" + hourOfDay;
        String minuteString = minute < 10 ? "0" + minute : "" + minute;
        tilTime.setText(hourString + ":" + minuteString);
        ECTime = hourString + ":" + minuteString;

    }

    public void doBack() {
        if (back == 1) {

            llMain.setVisibility(View.VISIBLE);
            llCClass.setVisibility(View.GONE);
            back = 0;
        } else {
            homeActivity.doMenuFirst();
        }
    }

    private void showProgress(final boolean show) {
        pb.setVisibility(show ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(show ? View.GONE : View.VISIBLE);
    }
}
