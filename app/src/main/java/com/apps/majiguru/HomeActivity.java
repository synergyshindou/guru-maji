package com.apps.majiguru;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.majiguru.Fragment.HomeFragment;
import com.apps.majiguru.Fragment.MengajiFragment;
import com.apps.majiguru.Fragment.MoreFragment;
import com.apps.majiguru.Fragment.NilaiFragment;
import com.apps.majiguru.Fragment.RiwayatFragment;
import com.apps.majiguru.Model.ChatAddUserResponse;
import com.apps.majiguru.Model.ChatGroupCreateResponse;
import com.apps.majiguru.Model.ChatLoginResponse;
import com.apps.majiguru.Model.ChatUnreadMsg;
import com.apps.majiguru.Model.Data;
import com.apps.majiguru.Model.DataProfile;
import com.apps.majiguru.Model.ProfileResponse;
import com.apps.majiguru.Model.ServerResponse;
import com.apps.majiguru.Model.UserCreateResponse;
import com.apps.majiguru.Rest.ApiConfig;
import com.apps.majiguru.Rest.AppConfig;
import com.apps.majiguru.Utils.Constant;
import com.apps.majiguru.Utils.MyApplication;
import com.apps.majiguru.Utils.SessionManager;
import com.apps.majiguru.Widget.NotifBadge;
import com.bumptech.glide.Glide;
import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.Socket;
import com.livinglifetechway.quickpermissions.annotations.WithPermissions;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    public static final int REQUEST_AUDIO_PERMISSION_CODE = 1;
    private static final String TAG = "HomeActivity";
    private static final String LOG_TAG = "AudioRecording";
    private static String mFileName = null;
    private final int PICK_IMAGE_REQUEST = 71;
    private final int SOUND_REQUEST = 72;
    public FragmentManager fm = getSupportFragmentManager();
    public SessionManager sessionManager;
    public String sToken, idUser;
    Fragment fragment1 = new HomeFragment();
    Fragment fragment2 = new MengajiFragment();
    Fragment fragment3 = new RiwayatFragment();
    Fragment fragment4 = new NilaiFragment();
    Fragment fragment5 = new MoreFragment();
    Fragment active = fragment1;
    Fragment activeShow = fragment1;

    ImageView imgH;
    public Data myData;
    DataProfile dataProfile;
    TextView tvMyStatus;
    View llStatus, llNilai;
    int selectAvailable = 0;
    AlertDialog.Builder builder;
    public Boolean isConnected = true;
    public Socket mSocket;

    public ApiConfig restApi;
    public ApiConfig restApiChat;
    private static final String[] MyStatus = new String[]{
            "Tidak aktif", "Aktif"
    };

    public TextView tvTitle, tvTopMsg;
    public BottomNavigationView bottomNavigationView;
    NotifBadge badge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);
        sessionManager = new SessionManager(this);
        restApi = AppConfig.getRetrofit().create(ApiConfig.class);
        restApiChat = AppConfig.getRetrofitChat().create(ApiConfig.class);

        String d = sessionManager.getData();
        myData = Data.create(d);

        sToken = sessionManager.getToken();
        idUser = myData.getId();

        if (!sessionManager.isTokenFCM())
            updateFCM();

        initViews();
//        fm.beginTransaction().add(R.id.frame_main, fragment6, "6").hide(fragment6).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment5, "5").hide(fragment5).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment4, "4").hide(fragment4).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment3, "3").hide(fragment3).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment2, "2").hide(fragment2).commit();
        fm.beginTransaction().add(R.id.frame_main, fragment1, "1").commit();

        methodWithPermissions();
        imgChange(1, 0, 0, 0, 0);

        if (getIntent().hasExtra("action")){
            if (getIntent().getStringExtra("action").equals("NILAI")){

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        // This method will be executed once the timer is over
                        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
                        fm.beginTransaction().show(fragment4).commit();
                        active = fragment4;
                        ((NilaiFragment) fragment4).getRaport(false);
                        imgChange(0, 0, 0, 1, 0);
                        bottomNavigationView.setSelectedItemId(R.id.llNilai);
                    }
                }, 1000);
            }
        }

//        getMyProfile();

        initChat();
    }

    void initViews() {
        tvMyStatus = findViewById(R.id.tvmystatus);
        llStatus = findViewById(R.id.llStatus);
        llNilai = findViewById(R.id.llNilai);
        tvTitle = findViewById(R.id.tvTitle);
        tvTopMsg = findViewById(R.id.tvTopMsg);

        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                onMenuClick(item);
                return true;
            }
        });

        imgH = findViewById(R.id.imgH);

        Glide.with(this)
                .load(R.drawable.headh)
                .into(imgH);
        Typeface face = Typeface.createFromAsset(getAssets(),
                "fonts/volterounded_semibold.otf");
        badge = findViewById(R.id.badges);
        badge.getTextView().setTypeface(face);
    }

    void initChat() {

        chatCreate();

        MyApplication app = (MyApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on(Constant.CHAT_MESSAGE, onChatMessage);
        mSocket.connect();
    }

    @Override
    public void onBackPressed() {
        if (fm.findFragmentByTag("4").isVisible()){
            ((NilaiFragment) active).doBack();
        } else if (fm.findFragmentByTag("2").isVisible()){
            ((MengajiFragment) active).doBack();
        } else if (fm.findFragmentByTag("3").isVisible()){
            ((RiwayatFragment) active).doBack();
        } else if (fm.findFragmentByTag("5").isVisible()){
            fm.popBackStack();
            ((MoreFragment) active).doBack();
        } else
            super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getMyProfile();
        Log.i(TAG, "onResume: "+isConnected);
        if (isConnected)
            chatUnread();
    }

    public void doNilai(View v){
        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
        fm.beginTransaction().show(fragment4).commit();
        active = fragment4;
        ((NilaiFragment) fragment4).getRaport(false);
        imgChange(0, 0, 0, 1, 0);
        bottomNavigationView.setSelectedItemId(R.id.llNilai);
    }

    public void doMenuFirst() {

        bottomNavigationView.setSelectedItemId(R.id.llHome);
        fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).show(fragment1).commit();
        active = fragment1;
        imgChange(1, 0, 0, 0, 0);
    }

    public void onMenuClick(MenuItem v) {
        if (v.getItemId() == R.id.llHome) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment1).commit();
            active = fragment1;
            ((HomeFragment) fragment1).getRaport(false);
            imgChange(1, 0, 0, 0, 0);
        }
        if (v.getItemId() == R.id.llMengaji) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment2).commit();
            active = fragment2;
            imgChange(0,1,0,0,0);
        }
        if (v.getItemId() == R.id.llRiwayat) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment3).commit();
            active = fragment3;
            imgChange(0, 0, 1, 0, 0);
        }
        if (v.getItemId() == R.id.llNilai) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment4).commit();
            active = fragment4;
            ((NilaiFragment) fragment4).getRaport(false);
            imgChange(0, 0, 0, 1, 0);
        }
        if (v.getItemId() == R.id.llMore) {
            fm.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE).hide(active).commit();
            fm.beginTransaction().show(fragment5).commit();
            active = fragment5;
            imgChange(0, 0, 0, 0, 1);
        }
    }

    public void doChat(View v){
        startActivity(new Intent(this, ChatActivity.class));
    }

    void imgChange(int a, int b, int c, int d, int e) {
        if (a == 1) {
            activeShow = fragment1;
            tvTitle.setText("Beranda");
            tvTopMsg.setVisibility(View.VISIBLE);
            imgH.setVisibility(View.VISIBLE);
        } else {
            tvTopMsg.setVisibility(View.GONE);
            imgH.setVisibility(View.GONE);
        }
        if (b == 1) {
            activeShow = fragment2;
            tvTitle.setText("Kelas");
        }
        if (c == 1) {
            activeShow = fragment2;
            tvTitle.setText("Riwayat");
        }
        if (d == 1) {
            activeShow = fragment4;
            tvTitle.setText("Nilai");
        }
        if (e == 1) {
            activeShow = fragment5;
            tvTitle.setText("Lainnya");
        }
    }

    public void animateImageView(final ImageView v, int os) {
//        final int orange;
//        if (os == 1) {
//            orange = getResources().getColor(R.color.Yalloew);
//        } else {
//            orange = getResources().getColor(R.color.white);
//        }
//
//        final ValueAnimator colorAnim = ObjectAnimator.ofFloat(0f, 1f);
//        colorAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                float mul = (Float) animation.getAnimatedValue();
//                int alphaOrange = adjustAlpha(orange, mul);
//                v.setColorFilter(alphaOrange, PorterDuff.Mode.SRC_ATOP);
//                if (mul == 0.0) {
//                    v.setColorFilter(null);
//                }
//            }
//        });
//
//        colorAnim.setDuration(100);
//        colorAnim.start();

    }

    public int adjustAlpha(int color, float factor) {
        int alpha = Math.round(Color.alpha(color) * factor);
        int red = Color.red(color);
        int green = Color.green(color);
        int blue = Color.blue(color);
        return Color.argb(alpha, red, green, blue);
    }

    public void openStatus(View v){
        showSingleChoiceDialog();
    }

    public void closeStatus(View v){
        llStatus.setVisibility(View.GONE);
    }

    public void getMyProfile(){
        restApi.myProfile(sToken, myData.getId()).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                    if (response.body().isStatus()) {
                        if (response.body().getDataProfile() != null) {
                            DataProfile data = response.body().getDataProfile();
                            if (data.getUsertype().equals("tutor")) {
                                String serialize = data.serialize();
                                Log.i(TAG, "onResponse: Data SERVER " + data.getStatusAvailable());
                                sessionManager.createDataProfile(serialize);
                                ((MoreFragment) fragment5).initProfile();

                                tvTopMsg.setText("Assalamualaikum, "+data.getName());
                                if (data.getUserImage() != null)
                                    if (data.getJk().equals("L")){
                                        if (data.getUserImage().equals("https://maghribmengaji.id/assets/images/icon_man.png")){
                                            showDialog("Mohon perbarui foto profil Anda!");
                                        }
                                    } else {
                                        if (data.getUserImage().equals("https://maghribmengaji.id/assets/images/icon_girl.png")){
                                            showDialog("Mohon perbarui foto profil Anda!");
                                        }
                                    }
                                else
                                    showDialog("Mohon perbarui foto profil Anda!");

                                if (data.getStatusAvailable().equals("0")){
                                    llStatus.setVisibility(View.VISIBLE);
                                    if (data.getStatusAvailable().equals("1")) {
                                        tvMyStatus.setText(MyStatus[Integer.parseInt(data.getStatusAvailable())]);
                                        tvMyStatus.setTextColor(Color.parseColor("#388E3C"));
                                    } else {
                                        tvMyStatus.setText(MyStatus[Integer.parseInt(data.getStatusAvailable())]);
                                        tvMyStatus.setTextColor(Color.parseColor("#D32F2F"));
                                    }
                                } else {
                                    llStatus.setVisibility(View.GONE);
                                }
                            }
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
                getMyProfile();

            }
        });
    }

    @WithPermissions(
            permissions = {Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE}
    )
    public void methodWithPermissions() {
        Log.i(TAG, "methodWithPermissions: ");
    }

    public void ShowDateFree(View view) {
        ((MengajiFragment) active).ShowDateFree(view);
    }

    public void ShowTimeFree(View v) {
        ((MengajiFragment) active).ShowTimeFree(v);
    }

    public void showDialog(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(str);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
        });
        builder.show();
    }

    public void showSingleChoiceDialog() {
        builder = new AlertDialog.Builder(HomeActivity.this);
        builder.setTitle("Tetapkan Status Saya");
        builder.setSingleChoiceItems(MyStatus, selectAvailable, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                selectAvailable = i;
            }
        });
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                setAvailable(selectAvailable);
            }
        });
        builder.setNegativeButton("Batal", null);
        builder.show();
    }

    void setAvailable(int a){
        restApi.setAvailable(sToken, idUser, a).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: " + response.body());
                    if (response.body().isSuccess()) {
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        getMyProfile();
                    } else
                        Toast.makeText(HomeActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "message = " + t.getMessage());
                Log.e(TAG, "cause = " + t.getCause());
                Toast.makeText(HomeActivity.this, "Please try again. . .", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void updateFCM(){
        restApi.sendFCM(myData.getId(), sessionManager.getTokenFCM(), sToken).enqueue(new Callback<ServerResponse>() {
            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
                Log.i(TAG, "onResponse: M " + response.message());
                if (response.body() != null) {
                    Log.i(TAG, "onResponse: B " + response.body());
                }
            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {

                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    void chatCreate() {
        restApiChat.chatCreate(myData.getId(), myData.getName(), myData.getName()).enqueue(new Callback<UserCreateResponse>() {
            @Override
            public void onResponse(Call<UserCreateResponse> call, Response<UserCreateResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        chatLogin();
                    }
                }
            }

            @Override
            public void onFailure(Call<UserCreateResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    void chatLogin() {
        restApiChat.chatLogin(myData.getId()).enqueue(new Callback<ChatLoginResponse>() {
            @Override
            public void onResponse(Call<ChatLoginResponse> call, Response<ChatLoginResponse> response) {
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        if (response.body().getStatus().equals("1")) {
                            sessionManager.createTokenChat(response.body().getApiToken());
                            chatCreateGroup();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatLoginResponse> call, Throwable t) {
                Log.e(TAG, "onFailure: " + t.getMessage());

            }
        });
    }

    void chatCreateGroup() {
            restApiChat.chatCreateGroup(myData.getName(), myData.getId()).enqueue(new Callback<ChatGroupCreateResponse>() {
                @Override
                public void onResponse(Call<ChatGroupCreateResponse> call, Response<ChatGroupCreateResponse> response) {
                    if (response.isSuccessful()){
                        if (response.body() != null){
                            Log.i(TAG, "onResponse: "+response.body().getMessage());
                            chatAddUser();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ChatGroupCreateResponse> call, Throwable t) {

                }
            });
    }

    void chatAddUser(){
        restApiChat.chatAddtoGroup(myData.getId(), myData.getId()).enqueue(new Callback<ChatAddUserResponse>() {
            @Override
            public void onResponse(Call<ChatAddUserResponse> call, Response<ChatAddUserResponse> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        Log.i(TAG, "onResponse: chatAddUser "+response.body());
                        chatUnread();
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatAddUserResponse> call, Throwable t) {

            }
        });
    }

    void chatUnread(){
        restApiChat.chatUnreadMsg(myData.getName(), myData.getId()).enqueue(new Callback<ChatUnreadMsg>() {
            @Override
            public void onResponse(Call<ChatUnreadMsg> call, Response<ChatUnreadMsg> response) {
                if (response.isSuccessful()){
                    if (response.body() != null){
                        Log.i(TAG, "onResponse: chatUnread "+response.body());
                        if (response.body().getMessageUnreads().size() > 0){
                            Log.i(TAG, "onResponse: chatUnread "+response.body().getMessageUnreads().toString());
                            if (response.body().getMessageUnreads().get(0).getUnread() > 0)
                                badge.setNumber(response.body().getMessageUnreads().get(0).getUnread(), true);
                            else badge.clear();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ChatUnreadMsg> call, Throwable t) {

            }
        });
    }

    // SOCKET EMIT

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "run: connected");
                    if (!isConnected) {
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "diconnected");
                    isConnected = false;
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };

    private Emitter.Listener onChatMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.i(TAG, "run: chatMessage");
                    chatUnread();
                }
            });
        }
    };
}
